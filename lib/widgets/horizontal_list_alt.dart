import 'package:bookshelves/models/Book.dart';
import 'package:bookshelves/models/Serie.dart';
import 'package:bookshelves/screens/books-details/books_details_screen.dart';
import 'package:bookshelves/utils/helper.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class HorizontalListAlt extends StatelessWidget {
  HorizontalListAlt({required this.list});
  final List<Book> list;
  // final List<int> numbers = [1, 2, 3, 5, 8, 13, 21, 34, 55];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 24.0, bottom: 24.0, left: 10.0),
      height: MediaQuery.of(context).size.height * 0.45,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: list.length,
        itemBuilder: (context, index) {
          var book = list[index];
          return Container(
            width: MediaQuery.of(context).size.width * 0.4,
            child: Stack(
              children: [
                Container(
                  margin: const EdgeInsets.all(5.0),
                  decoration: BoxDecoration(
                    color: Helper.hexaToColor(book.picture!.color!),
                    borderRadius: BorderRadius.all(
                      Radius.circular(5),
                    ),
                  ),
                  height: double.infinity,
                  width: double.infinity,
                  child: CachedNetworkImage(
                    imageUrl: book.picture!.base!,
                    imageBuilder: (context, imageProvider) => Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        image: DecorationImage(
                          image: imageProvider,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  child: Container(
                    margin: const EdgeInsets.all(5.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(5),
                      ),
                    ),
                    child: Material(
                      color: Colors.transparent,
                      child: InkWell(
                        highlightColor: Helper.hexaToColor(book.picture!.color!)
                            .withOpacity(0.5),
                        splashColor: Helper.hexaToColor(book.picture!.color!)
                            .withOpacity(0.5),
                        onTap: () {
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context) =>
                                  BooksDetailsScreen(book: book),
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  Widget _color(String hexacode) {
    var color = Helper.hexaToColor(hexacode);
    return SizedBox(
      child: DecoratedBox(
        decoration: BoxDecoration(color: color),
      ),
    );
  }
}
