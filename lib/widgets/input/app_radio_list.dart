import 'package:flutter/material.dart';

final GlobalKey<_AppRadioListState> appRadioListKey = GlobalKey();

class AppRadioList extends StatefulWidget {
  final String? title;
  final List<String> list;
  final ValueChanged<String> itemCallBack;
  final String? currentItem;
  final ScrollController? scrollController;

  AppRadioList({
    this.title,
    required this.list,
    required this.itemCallBack,
    this.currentItem,
    this.scrollController,
  });

  @override
  State<StatefulWidget> createState() => _AppRadioListState(currentItem);
}

class _AppRadioListState extends State<AppRadioList> {
  List<String> dropDownItems = [];
  String? currentItem;

  _AppRadioListState(this.currentItem);

  @override
  void initState() {
    super.initState();
    dropDownItems = widget.list;
  }

  @override
  void didUpdateWidget(AppRadioList oldWidget) {
    if (currentItem != widget.currentItem) {
      setState(() {
        currentItem = widget.currentItem!;
      });
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      controller: widget.scrollController,
      itemCount: dropDownItems.length,
      itemBuilder: (BuildContext context, int index) {
        var value = dropDownItems.elementAt(index);
        var uri = Uri.parse(value);
        return RadioListTile(
          value: value,
          groupValue: currentItem,
          onChanged: (selectedItem) => setState(() {
            currentItem = selectedItem as String;
            widget.itemCallBack(currentItem!);
          }),
          title: Text('$value'),
          subtitle: Text(
            uri.host,
            style: TextStyle(
              color: Colors.grey,
            ),
          ),
        );
      },
    );
  }
}
