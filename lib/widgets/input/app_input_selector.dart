// class AppSelector extends StatefulWidget {
//   final String label;
//   final TextInputType textInputType;
//   final String defaultValue;
//   TextEditingController controller;
//   final List<S2Choice<String>> choices;

//   AppSelector(
//       {Key key,
//       this.label = 'Sélectionner',
//       this.textInputType = TextInputType.text,
//       this.defaultValue,
//       this.choices,
//       this.controller})
//       : super(key: key);

//   @override
//   _AppSelectorState createState() => _AppSelectorState();
// }

// class _AppSelectorState extends State<AppSelector> {
//   String defaultValue;

//   @override
//   void initState() {
//     super.initState();
//     defaultValue = widget.defaultValue;
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: Card(
//         elevation: 1,
//         child: Column(
//           children: [
//             SmartSelect<String>.single(
//                 title: widget.label,
//                 value: widget.controller.text,
//                 onChange: (state) {
//                   setState(() => {
//                         defaultValue = state.value,
//                         widget.controller.text = state.value
//                       });
//                   print("original controller ${widget.controller.text}");
//                 },
//                 choiceItems: widget.choices,
//                 modalType: S2ModalType.bottomSheet,
//                 modalHeader: false,
//                 tileBuilder: (context, state) {
//                   return S2Tile.fromState(
//                     state,
//                     isTwoLine: true,
//                     title: Text(
//                       widget.label,
//                       style: TextStyle(
//                         color: Theme.of(context).primaryColor,
//                       ),
//                     ),
//                     // leading: const CircleAvatar(
//                     //   backgroundImage: NetworkImage(
//                     //       'https://source.unsplash.com/yeVtxxPxzbw/100x100'),
//                     // ),
//                   );
//                 }),
//           ],
//         ),
//       ),
//     );
//   }
// }
