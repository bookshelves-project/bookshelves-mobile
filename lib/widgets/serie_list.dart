import 'package:bookshelves/models/Author.dart';
import 'package:bookshelves/models/Serie.dart';
import 'package:bookshelves/repositories/SerieRepository.dart';
import 'package:bookshelves/screens/series-details/series_details_screen.dart';
import 'package:bookshelves/utils/helper.dart';
import 'package:bookshelves/utils/log.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class SeriesList extends StatefulWidget {
  SeriesList({Key? key}) : super(key: key);

  @override
  _SeriesListState createState() => _SeriesListState();
}

class _SeriesListState extends State<SeriesList> {
  late final _scrollController = ScrollController();
  late Future<List<Serie>> list;
  var currentList = <Serie>[];
  var loadCompleted = false;
  var isLoading = false;
  late String nextUrl;

  @override
  void initState() {
    super.initState();
    list = _getSeries();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      scrollLoader();
    });
  }

  void scrollLoader() {
    print('scrollLoader');
    _scrollController.addListener(() async {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        setState(() {
          list = _getSeries(route: nextUrl);
        });
      }
    });
    // _scrollController.addListener(
    //   () {
    //     if (_scrollController.offset >=
    //         _scrollController.position.maxScrollExtent) {
    //       print('reach to bottom botton');
    //       if (!loadCompleted) {
    //         setState(() {
    //           list = _getSeries(route: nextUrl);
    //         });
    //       }
    //     }
    //   },
    // );
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      isAlwaysShown: true,
      controller: _scrollController,
      child: RefreshIndicator(
        onRefresh: () => _onRefresh(),
        child: FutureBuilder<List<Serie>>(
          future: list,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              var list = snapshot.data!;
              // return ListCards(list: list);
              return Container(
                child: Scrollbar(
                  isAlwaysShown: true,
                  controller: _scrollController,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: ListView.builder(
                      controller: _scrollController,
                      itemBuilder: (context, index) {
                        final serie = list[index];
                        return CardSerie(serie: serie);
                      },
                      itemCount: list.length,
                    ),
                  ),
                ),
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ),
      ),
    );
  }

  Future<void> _onRefresh() async {
    // list = SerieRepository().getList();
  }

  Future<List<Serie>> _getSeries({String? route}) async {
    var list = <Serie>[];
    try {
      var response = await SerieRepository().getSeries(route: route);
      var currentPage = response.meta!.currentPage;
      var lastPage = response.meta!.lastPage;
      if (currentPage != (lastPage! + 1)) {
        nextUrl = response.links!.next!;
        print('currentPage: $currentPage - lastPage: $lastPage');
        print(response.links!.next);
        list = await SerieRepository().getSeriesList(laravelResponse: response);
        currentList = List.from(currentList)..addAll(list);
      } else {
        loadCompleted = true;
        print('loadCompleted');
      }

      print(currentList.length);
    } catch (e) {
      print(e.toString());
    }

    return currentList;
  }
}

class CardSerie extends StatelessWidget {
  const CardSerie({Key? key, required this.serie}) : super(key: key);

  final Serie serie;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
      elevation: 2,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            leading: CircleAvatar(
              backgroundImage: CachedNetworkImageProvider(serie.picture!.base!),
              backgroundColor: Helper.hexaToColor(serie.picture!.color!),
            ),
            title: Row(
              children: [
                Flexible(
                  child: Text(
                    serie.title!,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    softWrap: true,
                  ),
                ),
              ],
            ),
            trailing: Icon(Icons.keyboard_arrow_right),
            subtitle: Row(
              children: [
                Flexible(
                  child: Text(
                    'Write by ${Author.getAuthorList(serie.authors!)}',
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    softWrap: true,
                  ),
                ),
              ],
            ),
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => SeriesDetailsScreen(
                    serie: serie,
                  ),
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
