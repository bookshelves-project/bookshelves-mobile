import 'package:bookshelves/models/application/Choice.dart';
import 'package:bookshelves/routes/app_router.dart';
import 'package:bookshelves/utils/constants.dart';
import 'package:bookshelves/widgets/app_bottom_nav.dart';
import 'package:flutter/material.dart';

import 'app_drawer.dart';

String title = Constants.title;

class AppScaffold extends StatefulWidget {
  final String? title;
  final bool withDrawer;
  final Widget widget;

  const AppScaffold(
      {Key? key, this.title, required this.widget, this.withDrawer = false})
      : super(key: key);
  @override
  _AppScaffoldState createState() => _AppScaffoldState();
}

class _AppScaffoldState extends State<AppScaffold> {
  Choice _selectedChoice = choices[0]; // The app's "state".

  void _select(Choice choice) {
    // Causes the app to rebuild with the new _selectedChoice.
    setState(() {
      _selectedChoice = choice;
      Navigator.of(context).pushNamed(choice.route);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      // drawer: widget.withDrawer ? AppDrawer() : null,
      bottomNavigationBar: AppBottomNav(),
      appBar: AppBar(
        title: Text(
          widget.title ?? title,
          // style: GoogleFonts.handlee(fontSize: 20),
        ),
        elevation: 0,
        actions: <Widget>[
          // IconButton(
          //   icon: Icon(choices[0].icon),
          //   onPressed: () {
          //     _select(choices[0]);
          //   },
          // ),
          PopupMenuButton<Choice>(
            onSelected: (choice) => _select(choice),
            itemBuilder: (BuildContext context) {
              return choices.map((Choice choice) {
                return PopupMenuItem<Choice>(
                  value: choice,
                  child: Row(
                    children: [
                      Icon(choice.icon),
                      Padding(
                        padding: const EdgeInsets.only(left: 3.0),
                        child: Text(choice.title),
                      )
                    ],
                  ),
                );
              }).toList();
            },
          ),
        ],
      ),
      body: widget.widget,
    );
  }
}

const List<Choice> choices = <Choice>[
  // Choice(title: 'Search', icon: Icons.search),
  Choice(
    title: 'Settings',
    icon: Icons.settings,
    route: AppRouter.settingsRoute,
  ),
];

class ChoiceCard extends StatelessWidget {
  const ChoiceCard({Key? key, required this.choice}) : super(key: key);

  final Choice choice;

  @override
  Widget build(BuildContext context) {
    // final TextStyle textStyle = Theme.of(context).textTheme.display1;
    return Card(
      color: Colors.white,
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 12.0),
              child: Icon(choice.icon, size: 128.0),
            ),
            Text(choice.title),
          ],
        ),
      ),
    );
  }
}
