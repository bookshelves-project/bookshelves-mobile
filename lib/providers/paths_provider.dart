import 'dart:io';

import 'package:package_info/package_info.dart';
import 'package:path_provider/path_provider.dart' as path_provider;

class PathsProvider {
  static String androidExternalDirPath = '/storage/emulated/0';
  static String androidExternalDirDownloadPath =
      '$androidExternalDirPath/Download';

  static Future<List<FileSystemEntity>> getFilesList(
      {required String dir}) async {
    var fileList = <FileSystemEntity>[];
    fileList = Directory('$dir/').listSync();
    print(fileList);

    return fileList;
  }

  static void requestExternalStorageDirectories(
      path_provider.StorageDirectory type) async {
    var externalStorageDirectories =
        await path_provider.getExternalStorageDirectories(type: type);
    print(externalStorageDirectories);
  }

  static Future<String> getExternalStorageDownloadDirectory() async {
    var dirPath = '';
    if (Platform.isAndroid) {
      var extDir = await path_provider.getExternalStorageDirectory();
      dirPath = '${extDir!.path}/Download';
      var packageInfo = await PackageInfo.fromPlatform();
      dirPath = dirPath.replaceAll(
          'Android/data/${packageInfo.packageName}/files/', '');
    }
    return dirPath;
  }

  static Future<Map<String, dynamic>> getPathWithPathProvider() async {
    var paths = <String, dynamic>{};
    try {
      paths.addAll({
        'getApplicationDocumentsDirectory':
            await path_provider.getApplicationDocumentsDirectory()
      });
    } catch (e) {}
    try {
      paths.addAll({
        'getApplicationSupportDirectory':
            await path_provider.getApplicationSupportDirectory()
      });
    } catch (e) {}
    try {
      paths.addAll({
        'getDownloadsDirectory': await path_provider.getDownloadsDirectory()
      });
    } catch (e) {}
    try {
      paths.addAll({
        'getExternalCacheDirectories':
            await path_provider.getExternalCacheDirectories()
      });
    } catch (e) {}
    try {
      paths.addAll({
        'getExternalStorageDirectories':
            await path_provider.getExternalStorageDirectories()
      });
    } catch (e) {}
    try {
      paths.addAll({
        'getExternalStorageDirectory':
            await path_provider.getExternalStorageDirectory()
      });
    } catch (e) {}
    try {
      paths.addAll(
          {'getLibraryDirectory': await path_provider.getLibraryDirectory()});
    } catch (e) {}
    try {
      paths.addAll({
        'getTemporaryDirectory': await path_provider.getTemporaryDirectory()
      });
    } catch (e) {}

    return paths;
  }
}
