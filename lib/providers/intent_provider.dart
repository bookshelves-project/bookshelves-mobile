import 'dart:io';

import 'package:android_intent/android_intent.dart';
import 'package:android_intent/flag.dart';
import 'package:bookshelves/providers/paths_provider.dart';
import 'package:bookshelves/utils/log.dart';
import 'package:package_info/package_info.dart';
import 'package:path_provider/path_provider.dart';

class IntentProvider {
  static void openFileManager({required String dir}) async {
    try {
      if (Platform.isAndroid) {
        var path = Uri.parse(PathsProvider.androidExternalDirDownloadPath).path;

        print(path);
        var package = await PackageInfo.fromPlatform();
        var name = package.packageName;
        var chromePackage = 'com.android.chrome';
        var keepPackage = 'com.google.android.keep';
        var intentKeepStore = AndroidIntent(
          action: 'action_view',
          data: Uri.encodeFull(
            // 'market://details?id=$keepPackage',
            'https://play.google.com/store/apps/details?id=$keepPackage',
          ),
        );

        var intent = AndroidIntent(
          action: IntentAction.view.value,
          type: IntentType.directory.value,
          // data: Uri.encodeFull('https://flutter.io'),
          // flags: <int>[Flag.FLAG_ACTIVITY_NEW_TASK],
          // type: IntentType.any.value,
          // data: 'package:$name',
        );
        await intent.launch();
      }
    } catch (e) {
      Log.warn(e.toString());
    }
  }
}

enum IntentAction {
  view,
  get_content,
  location_source_settings,
  application_details_settings
}
enum IntentType { directory, any }

extension IntentActionExtension on IntentAction {
  String get value {
    switch (this) {
      case IntentAction.view:
        return 'action_view';
      case IntentAction.get_content:
        return 'action_get_content';
      case IntentAction.location_source_settings:
        return 'action_location_source_settings';
      case IntentAction.application_details_settings:
        return 'action_application_details_settings';
    }
  }
}

extension IntentTypeExtension on IntentType {
  String get value {
    switch (this) {
      case IntentType.directory:
        return 'vnd.android.document/directory';
      case IntentType.any:
        return '*/*';
    }
  }
}
