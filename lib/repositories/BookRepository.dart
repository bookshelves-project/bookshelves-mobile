import 'package:bookshelves/models/Book.dart';
import 'package:bookshelves/providers/api_provider.dart';
import 'package:bookshelves/providers/api_routes.dart';

class BookRepository {
  Future<Book> get(Book book) async {
    var entity = Book();

    var response = await ApiProvider().getHTTP(book.meta!.show!);
    var data = response!.data['data'];

    try {
      entity = Book.fromJson(data);
    } catch (e) {
      print(e.toString());
    }

    return entity;
  }

  Future<List<Book>> getSelection() async {
    var list = <Book>[];

    var response = await ApiProvider().getHTTP(ApiRoutes.booksSelectionRoute);
    var data = response!.data['data'];

    try {
      for (var item in data) {
        var book = Book.fromJson(item);
        list.add(book);
      }
    } catch (e) {
      print(e.toString());
    }

    return list;
  }
}
