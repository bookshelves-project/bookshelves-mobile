import 'package:bookshelves/models/LaravelResponse.dart';
import 'package:bookshelves/models/Serie.dart';
import 'package:bookshelves/providers/api_provider.dart';
import 'package:bookshelves/providers/api_routes.dart';

class SerieRepository {
  Future<LaravelResponse> getSeries({String? route}) async {
    route ??= ApiRoutes.seriesRoute;
    var laravelResponse = LaravelResponse();

    var response = await ApiProvider().getHTTP(route);
    var data = response!.data;

    if (response.statusCode == 200) {
      laravelResponse = LaravelResponse.fromJson(data);

      return laravelResponse;
    } else {
      throw Exception('Failed to load laravelResponse');
    }
  }

  Future<List<Serie>> getSeriesList(
      {required LaravelResponse laravelResponse}) async {
    var list = <Serie>[];

    for (var serieRaw in laravelResponse.data!) {
      var serie = Serie.fromJson(serieRaw);
      list.add(serie);
    }

    return list;
  }

  Future<List<Serie>> getSeriesAlt() async {
    var list = <Serie>[];
    var laravelResponse = LaravelResponse();
    String nextUrl;
    bool loadCompleted;

    var response = await ApiProvider().getHTTP(ApiRoutes.seriesRoute);
    var data = response!.data;

    if (response.statusCode == 200) {
      laravelResponse = LaravelResponse.fromJson(data);

      if (laravelResponse.links!.next != null) {
        nextUrl = laravelResponse.links!.next!;
      } else {
        loadCompleted = true;
      }
      for (var serieRaw in laravelResponse.data!) {
        var serie = Serie.fromJson(serieRaw);
        list.add(serie);
      }
      return list;
    } else {
      throw Exception('Failed to load Test');
    }
  }

  Future<List<Serie>> getList({
    required LaravelResponse laravelResponse,
    required bool next,
  }) async {
    var list = <Serie>[];
    LaravelResponse laravelResponse;

    var response = await ApiProvider().getHTTP(ApiRoutes.seriesRoute);
    var data = response!.data;

    try {
      laravelResponse = LaravelResponse.fromJson(data);
      for (var serieRaw in laravelResponse.data!) {
        var serie = Serie.fromJson(serieRaw);
        list.add(serie);
      }
    } catch (e) {
      print(e.toString());
    }

    return list;
  }

  Future<Serie> get(String url) async {
    var entity = Serie();

    var response = await ApiProvider().getHTTP(url);
    var data = response!.data['data'];

    try {
      entity = Serie.fromJson(data);
    } catch (e) {
      print(e.toString());
    }

    return entity;
  }
}
