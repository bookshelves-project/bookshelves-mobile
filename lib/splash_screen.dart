import 'dart:convert';

import 'package:bookshelves/bookshelves.dart';
import 'package:bookshelves/providers/preferences_provider.dart';
import 'package:bookshelves/routes/app_router.dart';
import 'package:bookshelves/theme/theme_manager.dart';
import 'package:bookshelves/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  var _theme;
  Future<String?> _settings() async {
    var prefs = await SharedPreferences.getInstance();
    // await prefs.clear();
    var apiList = await prefs.getString('apiList');
    if (apiList == null) {
      await prefs.setString('apiList', jsonEncode(Constants.apiList));
    }
    apiList = await prefs.getString('apiList');
    var api = await prefs.getString('api');
    // print('api');
    // print(api);

    var theme = ThemeNotifier();

    // Get theme of device
    var brightness = SchedulerBinding.instance!.window.platformBrightness;
    var darkModeOn = brightness == Brightness.dark;

    var themeMode = await PreferencesProvider.readData('themeMode');

    // Manage for first init
    if (themeMode == null) {
      if (darkModeOn) {
        theme.setDarkMode();
      } else {
        theme.setLightMode();
      }
    } else {
      if (themeMode == 'light') {
        theme.setLightMode();
      } else {
        theme.setDarkMode();
      }
    }

    _theme = theme.getTheme();

    return api ?? '';
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ThemeNotifier>(
      builder: (context, theme, _) => FutureBuilder(
        future: _settings(),
        builder: (context, snapshot) {
          return MaterialApp(
            theme: _theme,
            color: Color(primaryHexa),
            home: _home(snapshot),
            debugShowCheckedModeBanner: false,
            onGenerateRoute: AppRouter.generateRoute,
            initialRoute: '/',
          );
        },
      ),
    );
    // return Consumer<ThemeNotifier>(
    //   builder: (context, theme, _) => MaterialApp(
    // theme: theme.getTheme(),
    // color: Color(primaryHexa),
    // home: SplashScreen(),
    // debugShowCheckedModeBanner: false,
    // onGenerateRoute: AppRouter.generateRoute,
    // initialRoute: '/',
    //   ),
    // );
    // return FutureBuilder<String?>(
    //   future: _settings(),
    //   builder: (context, snapshot) {
    // if (snapshot.hasData) {
    //   return Bookshelves();
    // } else {
    //   return _splashScreen();
    // }
    //   },
    // );
  }

  Widget _splashScreen() {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image(
              image: AssetImage('assets/images/icon.png'),
              // width: 300,
              height: 150,
              fit: BoxFit.fill,
            ),
            Padding(
              padding: EdgeInsets.only(top: 5),
              child: Text(
                Constants.title,
                textAlign: TextAlign.center,
                style: GoogleFonts.handlee(
                  fontSize: 48,
                  fontWeight: FontWeight.w700,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  _home(AsyncSnapshot snapshot) {
    if (snapshot.hasData) {
      return Bookshelves();
    } else {
      return _splashScreen();
    }
  }
}
