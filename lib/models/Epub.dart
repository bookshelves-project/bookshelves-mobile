import 'package:json_annotation/json_annotation.dart';

part 'Epub.g.dart';

@JsonSerializable()
class Epub {
  Epub({
    this.name,
    this.size,
    this.download,
  });

  String? name;
  String? size;
  String? download;

  factory Epub.fromJson(Map<String, dynamic> json) => _$EpubFromJson(json);
  Map<String, dynamic> toJson() => _$EpubToJson(this);
}
