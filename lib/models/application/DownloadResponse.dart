import 'package:json_annotation/json_annotation.dart';

part 'DownloadResponse.g.dart';

@JsonSerializable()
class DownloadResponse {
  DownloadResponse({
    required this.success,
    this.path,
    this.filename,
  });

  bool success;
  String? path;
  String? filename;

  factory DownloadResponse.fromJson(Map<String, dynamic> json) =>
      _$DownloadResponseFromJson(json);
  Map<String, dynamic> toJson() => _$DownloadResponseToJson(this);
}
