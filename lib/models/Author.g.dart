// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Author.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Author _$AuthorFromJson(Map<String, dynamic> json) {
  return Author(
    name: json['name'] as String?,
    slug: json['slug'] as String?,
    meta: json['meta'] == null
        ? null
        : Meta.fromJson(json['meta'] as Map<String, dynamic>),
    lastname: json['lastname'] as String?,
    firstname: json['firstname'] as String?,
    picture: json['picture'] == null
        ? null
        : Picture.fromJson(json['picture'] as Map<String, dynamic>),
    count: json['count'] as int?,
    description: json['description'] as String?,
    link: json['link'] as String?,
    size: json['size'] as String?,
    download: json['download'] as String?,
    series: (json['series'] as List<dynamic>?)
        ?.map((e) => Serie.fromJson(e as Map<String, dynamic>))
        .toList(),
    books: (json['books'] as List<dynamic>?)
        ?.map((e) => Book.fromJson(e as Map<String, dynamic>))
        .toList(),
    isFavorite: json['isFavorite'] as bool?,
  );
}

Map<String, dynamic> _$AuthorToJson(Author instance) => <String, dynamic>{
      'name': instance.name,
      'slug': instance.slug,
      'meta': instance.meta,
      'lastname': instance.lastname,
      'firstname': instance.firstname,
      'picture': instance.picture,
      'count': instance.count,
      'description': instance.description,
      'link': instance.link,
      'size': instance.size,
      'download': instance.download,
      'series': instance.series,
      'books': instance.books,
      'isFavorite': instance.isFavorite,
    };
