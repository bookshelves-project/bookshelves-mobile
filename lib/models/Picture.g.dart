// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Picture.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Picture _$PictureFromJson(Map<String, dynamic> json) {
  return Picture(
    base: json['base'] as String?,
    openGraph: json['openGraph'] as String?,
    original: json['original'] as String?,
    color: json['color'] as String?,
  );
}

Map<String, dynamic> _$PictureToJson(Picture instance) => <String, dynamic>{
      'base': instance.base,
      'openGraph': instance.openGraph,
      'original': instance.original,
      'color': instance.color,
    };
