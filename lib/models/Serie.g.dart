// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Serie.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Serie _$SerieFromJson(Map<String, dynamic> json) {
  return Serie(
    title: json['title'] as String?,
    slug: json['slug'] as String?,
    author: json['author'] as String?,
    picture: json['picture'] == null
        ? null
        : Picture.fromJson(json['picture'] as Map<String, dynamic>),
    meta: json['meta'] == null
        ? null
        : Meta.fromJson(json['meta'] as Map<String, dynamic>),
    language: json['language'] as String?,
    authors: (json['authors'] as List<dynamic>?)
        ?.map((e) => Author.fromJson(e as Map<String, dynamic>))
        .toList(),
    count: json['count'] as int?,
    description: json['description'] as String?,
    link: json['link'] as String?,
    tags: (json['tags'] as List<dynamic>?)
        ?.map((e) => Tag.fromJson(e as Map<String, dynamic>))
        .toList(),
    download: json['download'] as String?,
    size: json['size'] as String?,
    books: (json['books'] as List<dynamic>?)
            ?.map((e) => Book.fromJson(e as Map<String, dynamic>))
            .toList() ??
        [],
    isFavorite: json['isFavorite'] as bool?,
  );
}

Map<String, dynamic> _$SerieToJson(Serie instance) => <String, dynamic>{
      'title': instance.title,
      'slug': instance.slug,
      'author': instance.author,
      'picture': instance.picture,
      'meta': instance.meta,
      'language': instance.language,
      'authors': instance.authors,
      'count': instance.count,
      'description': instance.description,
      'link': instance.link,
      'tags': instance.tags,
      'download': instance.download,
      'size': instance.size,
      'books': instance.books,
      'isFavorite': instance.isFavorite,
    };
