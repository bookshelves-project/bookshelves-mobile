import 'package:json_annotation/json_annotation.dart';

part 'Identifier.g.dart';

@JsonSerializable()
class Identifier {
  Identifier({
    this.isbn,
    this.isbn13,
    this.doi,
    this.amazon,
    this.google,
  });

  String? isbn;
  String? isbn13;
  String? doi;
  String? amazon;
  String? google;

  factory Identifier.fromJson(Map<String, dynamic> json) =>
      _$IdentifierFromJson(json);
  Map<String, dynamic> toJson() => _$IdentifierToJson(this);
}
