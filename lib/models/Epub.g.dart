// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Epub.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Epub _$EpubFromJson(Map<String, dynamic> json) {
  return Epub(
    name: json['name'] as String?,
    size: json['size'] as String?,
    download: json['download'] as String?,
  );
}

Map<String, dynamic> _$EpubToJson(Epub instance) => <String, dynamic>{
      'name': instance.name,
      'size': instance.size,
      'download': instance.download,
    };
