import 'package:json_annotation/json_annotation.dart';

part 'GoogleBook.g.dart';

@JsonSerializable()
class GoogleBook {
  GoogleBook({
    this.previewLink,
    this.buyLink,
    this.retailPrice,
    this.retailPriceCurrency,
    this.createdAt,
  });

  String? previewLink;
  String? buyLink;
  int? retailPrice;
  String? retailPriceCurrency;
  DateTime? createdAt;

  factory GoogleBook.fromJson(Map<String, dynamic> json) =>
      _$GoogleBookFromJson(json);
  Map<String, dynamic> toJson() => _$GoogleBookToJson(this);
}
