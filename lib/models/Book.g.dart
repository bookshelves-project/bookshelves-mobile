// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Book.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Book _$BookFromJson(Map<String, dynamic> json) {
  return Book(
    title: json['title'] as String?,
    slug: json['slug'] as String?,
    author: json['author'] as String?,
    authors: (json['authors'] as List<dynamic>?)
        ?.map((e) => Author.fromJson(e as Map<String, dynamic>))
        .toList(),
    summary: json['summary'] as String?,
    language: json['language'] as String?,
    publishDate: json['publishDate'] == null
        ? null
        : DateTime.parse(json['publishDate'] as String),
    picture: json['picture'] == null
        ? null
        : Picture.fromJson(json['picture'] as Map<String, dynamic>),
    publisher: json['publisher'] == null
        ? null
        : Publisher.fromJson(json['publisher'] as Map<String, dynamic>),
    volume: json['volume'] as int?,
    meta: json['meta'] == null
        ? null
        : Meta.fromJson(json['meta'] as Map<String, dynamic>),
    serie: json['serie'] == null
        ? null
        : Serie.fromJson(json['serie'] as Map<String, dynamic>),
    description: json['description'] as String?,
    identifier: json['identifier'] == null
        ? null
        : Identifier.fromJson(json['identifier'] as Map<String, dynamic>),
    pageCount: json['pageCount'] as int?,
    maturityRating: json['maturityRating'] as String?,
    tags: (json['tags'] as List<dynamic>?)
        ?.map((e) => Tag.fromJson(e as Map<String, dynamic>))
        .toList(),
    epub: json['epub'] == null
        ? null
        : Epub.fromJson(json['epub'] as Map<String, dynamic>),
    googleBook: json['googleBook'] == null
        ? null
        : GoogleBook.fromJson(json['googleBook'] as Map<String, dynamic>),
    isFavorite: json['isFavorite'] as bool?,
  );
}

Map<String, dynamic> _$BookToJson(Book instance) => <String, dynamic>{
      'title': instance.title,
      'slug': instance.slug,
      'author': instance.author,
      'authors': instance.authors,
      'summary': instance.summary,
      'language': instance.language,
      'publishDate': instance.publishDate?.toIso8601String(),
      'picture': instance.picture,
      'publisher': instance.publisher,
      'volume': instance.volume,
      'meta': instance.meta,
      'serie': instance.serie,
      'description': instance.description,
      'identifier': instance.identifier,
      'pageCount': instance.pageCount,
      'maturityRating': instance.maturityRating,
      'tags': instance.tags,
      'epub': instance.epub,
      'googleBook': instance.googleBook,
      'isFavorite': instance.isFavorite,
    };
