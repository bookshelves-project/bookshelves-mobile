// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Publisher.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Publisher _$PublisherFromJson(Map<String, dynamic> json) {
  return Publisher(
    name: json['name'] as String?,
    slug: json['slug'] as String?,
  );
}

Map<String, dynamic> _$PublisherToJson(Publisher instance) => <String, dynamic>{
      'name': instance.name,
      'slug': instance.slug,
    };
