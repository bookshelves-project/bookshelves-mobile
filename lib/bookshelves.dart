import 'package:bookshelves/models/application/Choice.dart';
import 'package:bookshelves/routes/app_router.dart';
import 'package:bookshelves/screens/home/home_screen.dart';
import 'package:bookshelves/screens/series/series_screen.dart';
import 'package:bookshelves/theme/theme.dart';
import 'package:bookshelves/widgets/app_bottom_nav.dart';
import 'package:bookshelves/widgets/app_scaffold.dart';
import 'package:flutter/material.dart';

class Bookshelves extends StatefulWidget {
  Bookshelves({Key? key}) : super(key: key);

  @override
  _BookshelvesState createState() => _BookshelvesState();
}

class _BookshelvesState extends State<Bookshelves> {
  late PageController _pageController;
  var _selectedPage = 0;

  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: _selectedPage);
  }

  _select(choice) {
    Navigator.of(context).pushNamed(choice.route);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        title: Text('App'),
        backgroundColor: Colors.transparent,
        elevation: 0,
        actions: [
          PopupMenuButton<Choice>(
            onSelected: (choice) => _select(choice),
            itemBuilder: (BuildContext context) {
              return choices.map((Choice choice) {
                return PopupMenuItem<Choice>(
                  value: choice,
                  child: Row(
                    children: [
                      Icon(choice.icon),
                      Padding(
                        padding: const EdgeInsets.only(left: 3.0),
                        child: Text(choice.title),
                      )
                    ],
                  ),
                );
              }).toList();
            },
          ),
        ],
      ),
      body: PageView(
        physics: NeverScrollableScrollPhysics(),
        controller: _pageController,
        children: <Widget>[
          HomeScreen(),
          SeriesScreen(),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _selectedPage,
        // backgroundColor: primary,
        // selectedItemColor: Colors.white,
        // unselectedItemColor: Colors.white.withOpacity(.60),
        // selectedFontSize: 14,
        // unselectedFontSize: 14,
        // onTap: (value) {
        //   print(value);
        //   print(_children[value].route);
        //   if (ModalRoute.of(context)!.settings.name != _children[value].route) {
        //     Navigator.of(context).pushNamed(_children[value].route);
        //   }
        //   setState(() {
        //     _currentIndex = value;
        //   });
        // },
        onTap: (value) {
          print(value);
          _pageController.jumpToPage(value);
          setState(() {
            _selectedPage = value;
          });
        },
        items: _list(),
      ),
    );
  }

  List<NavigationBarItem> _list() {
    return [
      NavigationBarItem(
        label: 'Home',
        icon: Icon(
          Icons.home,
        ),
        route: AppRouter.homeRoute,
      ),
      NavigationBarItem(
        label: 'Series',
        icon: Icon(
          Icons.collections,
        ),
        route: AppRouter.seriesRoute,
      ),
    ];
  }
}
