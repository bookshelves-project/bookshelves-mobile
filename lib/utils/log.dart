import 'package:logger/logger.dart';

class Log {
  /// Return simple logger
  Logger get logger {
    return Logger(
      printer: PrettyPrinter(methodCount: 0, lineLength: 50),
    );
  }

  static void warn(dynamic message) {
    message = message.toString();
    Log().logger.w(message);
  }

  static void info(dynamic message) {
    message = message.toString();
    Log().logger.i(message);
  }

  static void danger(dynamic message) {
    message = message.toString();
    Log().logger.d(message);
  }
}
