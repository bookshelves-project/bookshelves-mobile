import 'package:bookshelves/models/Author.dart';
import 'package:bookshelves/models/Book.dart';
import 'package:bookshelves/models/Serie.dart';
import 'package:bookshelves/repositories/SerieRepository.dart';
import 'package:bookshelves/screens/books-details/books_details_screen.dart';
import 'package:bookshelves/utils/helper.dart';
import 'package:bookshelves/widgets/app_scaffold.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class SeriesDetailsScreen extends StatefulWidget {
  SeriesDetailsScreen({Key? key, required this.serie}) : super(key: key);
  final Serie serie;

  @override
  _SeriesDetailsScreenState createState() => _SeriesDetailsScreenState();
}

class _SeriesDetailsScreenState extends State<SeriesDetailsScreen> {
  late Serie serie = widget.serie;
  late Future<Serie> future;

  @override
  void initState() {
    super.initState();
    future = SerieRepository().get(serie.meta!.show!);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(serie.title!),
      ),
      body: RefreshIndicator(
        onRefresh: () => _onRefresh(),
        child: FutureBuilder<Serie>(
          future: future,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              var serie = snapshot.data!;
              return SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 10.0,
                    vertical: 15.0,
                  ),
                  child: Column(
                    children: [
                      Container(
                        child: Column(
                          children: [
                            ListTile(
                              leading: CachedNetworkImage(
                                imageUrl: serie.picture!.base!,
                                placeholder: (context, url) =>
                                    _color(serie.picture!.color!),
                                imageBuilder: (context, imageProvider) =>
                                    Container(
                                  width: 50.0,
                                  height: 50.0,
                                  decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5)),
                                    image: DecorationImage(
                                      image: imageProvider,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                              ),
                              title: Text(serie.title!),
                              subtitle: Text(
                                Author.getAuthorList(serie.authors!),
                                style: TextStyle(
                                    color: Colors.black.withOpacity(0.6)),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Text(
                                serie.description!,
                                textAlign: TextAlign.justify,
                                style: TextStyle(
                                  color: Colors.black.withOpacity(0.6),
                                ),
                              ),
                            ),
                            ButtonBar(
                              alignment: MainAxisAlignment.start,
                              children: [
                                OutlinedButton(
                                  onPressed: () {
                                    // Perform some action
                                  },
                                  child: Row(
                                    children: [
                                      Icon(Icons.download_rounded),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 3.0),
                                        child: const Text('Download'),
                                      )
                                    ],
                                  ),
                                ),
                                OutlinedButton(
                                  onPressed: () {
                                    // Perform some action
                                  },
                                  child: Row(
                                    children: [
                                      Icon(Icons.link),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 3.0),
                                        child: const Text('More infos'),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 20.0),
                        child: Text(
                          '${serie.count} Books',
                          style: TextStyle(fontWeight: FontWeight.w700),
                        ),
                      ),
                      ListBooksGridHorizontal(
                        list: serie.books!,
                      )
                    ],
                  ),
                ),
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ),
      ),
    );
  }

  _onRefresh() {}

  Widget _color(String hexacode) {
    var color = Helper.hexaToColor(hexacode);
    return SizedBox(
      child: DecoratedBox(
        decoration: BoxDecoration(color: color),
      ),
    );
  }

  Widget _listBooks({required List<Book> list}) {
    return SizedBox(
      height: 150,
      child: ListView.builder(
        itemExtent: 150,
        scrollDirection: Axis.horizontal,
        itemCount: list.length,
        itemBuilder: (context, index) {
          var book = list[index];
          return Container(
            margin: EdgeInsets.all(5.0),
            color: Colors.yellow,
            child: CachedNetworkImage(
              height: 100.0,
              imageUrl: book.picture!.base!,
              placeholder: (context, url) => _color(book.picture!.color!),
              fit: BoxFit.cover,
              width: double.infinity,
              imageBuilder: (context, imageProvider) => Container(
                width: 80.0,
                height: 80.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}

class ListBooksGridHorizontal extends StatelessWidget {
  final List<Book> list;
  const ListBooksGridHorizontal({Key? key, required this.list})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: list.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        crossAxisSpacing: 10.0,
        mainAxisSpacing: 10.0,
      ),
      itemBuilder: (BuildContext context, int index) {
        var book = list[index];
        return Stack(
          children: [
            GridTile(
              child: CachedNetworkImage(
                placeholder: (context, url) => Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    color: Helper.hexaToColor(
                      book.picture!.color!,
                    ),
                  ),
                ),
                imageUrl: book.picture!.base!,
                imageBuilder: (context, imageProvider) => Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    image: DecorationImage(
                      image: imageProvider,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              header: Container(
                padding: const EdgeInsets.all(6.0),
                // color: Colors.black,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: <Color>[
                      Helper.hexaToColor(
                        book.picture!.color!,
                      ),
                      Helper.hexaToColor(
                        book.picture!.color!,
                      ).withOpacity(0),
                    ],
                    // stops: [0.0, 1.0],
                    tileMode: TileMode.clamp,
                  ),
                ),
                child: Text(
                  'Vol. ${book.volume!}',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 12,
                    shadows: [
                      Shadow(
                        color: Colors.black,
                        offset: Offset(1, 1),
                        blurRadius: 1,
                      )
                    ],
                  ),
                ),
              ),
              footer: Container(
                padding: const EdgeInsets.all(6.0),
                // color: Colors.black,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    end: Alignment.topCenter,
                    begin: Alignment.bottomCenter,
                    colors: <Color>[
                      Helper.hexaToColor(
                        book.picture!.color!,
                      ),
                      Helper.hexaToColor(
                        book.picture!.color!,
                      ).withOpacity(0),
                    ],
                    stops: [0.0, 1.0],
                    tileMode: TileMode.clamp,
                  ),
                ),
                child: Text(
                  '${book.title!}',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 12,
                    shadows: [
                      Shadow(
                        color: Colors.black,
                        offset: Offset(1, 1),
                        blurRadius: 1,
                      )
                    ],
                  ),
                ),
              ),
              // child: Column(
              //   children: [
              //     CachedNetworkImage(
              //       imageUrl: book.picture!.base!,
              //       imageBuilder: (context, imageProvider) => Container(
              //         decoration: BoxDecoration(
              //           borderRadius: BorderRadius.all(Radius.circular(5)),
              //           image: DecorationImage(
              //             image: imageProvider,
              //             fit: BoxFit.cover,
              //           ),
              //         ),
              //       ),
              //     ),
              //   ],
              // ),
            ),
            Positioned(
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                ),
                child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => BooksDetailsScreen(book: book),
                        ),
                      );
                    },
                  ),
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}
