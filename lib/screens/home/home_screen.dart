import 'package:bookshelves/bookshelves.dart';
import 'package:bookshelves/models/Book.dart';
import 'package:bookshelves/repositories/BookRepository.dart';
import 'package:bookshelves/routes/app_router.dart';
import 'package:bookshelves/screens/settings/widgets/settings_api.dart';
import 'package:bookshelves/utils/constants.dart';
import 'package:bookshelves/widgets/card_custom.dart';
import 'package:bookshelves/widgets/horizontal_list_alt.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

final GlobalKey<_HomeScreenState> homeKey = GlobalKey();

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late Future<bool> _apiIsSet;
  late Future<List<Book>> _futureBooksSelectionList;

  @override
  void initState() {
    super.initState();
    _apiIsSet = _getSharedPreferences();
    _futureBooksSelectionList = BookRepository().getSelection();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 60.0),
        child: Column(
          children: [
            FutureBuilder<bool>(
              initialData: false,
              future: _apiIsSet,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  if (snapshot.data!) {
                    return Container();
                  } else {
                    return _sourceChoiceCard();
                  }
                } else {
                  return Container();
                }
              },
            ),
            FutureBuilder<List<Book>>(
              future: _futureBooksSelectionList,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  var list = snapshot.data!;
                  return HorizontalListAlt(list: list);
                  // return Container(
                  //   margin: const EdgeInsets.symmetric(vertical: 100.0),
                  //   child: CircularProgressIndicator(),
                  // );
                } else {
                  return Container(
                    margin: const EdgeInsets.symmetric(vertical: 135.0),
                    child: CircularProgressIndicator(),
                  );
                }
              },
            ),
            Container(
              width: double.infinity,
              child: Text(
                'Welcome on\nBookshelves',
                textAlign: TextAlign.center,
              ),
            )
          ],
        ),
        // decoration: BoxDecoration(
        //   color: Colors.grey[100],
        //   image: DecorationImage(
        //     image: AssetImage('assets/images/icon.jpg'),
        //     fit: BoxFit.cover,
        //     colorFilter: ColorFilter.mode(
        //         Colors.black.withOpacity(0.1), BlendMode.dstATop),
        //   ),
        // ),
      ),
    );
  }

  CardCustom _sourceChoiceCard() {
    return CardCustom(
      leading: Icon(
        Icons.warning_rounded,
        size: 30.0,
      ),
      title: Text('Default source defined'),
      subtitle: Text('Check source into settings'),
      text:
          'Bookshelves can use different sources to get eBooks, you can choose default source, but you can too discover other sources of eBooks. You can choose a new source into application settings, from a pre-defined list or add your own source.',
      firstButton: TextButton(
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => SettingsApi(),
            ),
          );
        },
        child: const Text('SELECT A SOURCE'),
      ),
      secondButton: TextButton(
        onPressed: () async {
          var prefs = await SharedPreferences.getInstance();
          await prefs.setBool('apiIsSet', true);
          setState(() {
            _apiIsSet = _getSharedPreferences();
          });
          // await Navigator.of(context).pushAndRemoveUntil(
          //   MaterialPageRoute(
          //     builder: (context) => Bookshelves(),
          //   ),
          //   (route) => false,
          // );
        },
        child: const Text('CANCEL'),
      ),
    );
  }

  Future<bool> _getSharedPreferences() async {
    var apiIsSet = false;
    var prefs = await SharedPreferences.getInstance();
    var prefsApiIsSet = prefs.getBool('apiIsSet');
    if (prefsApiIsSet != null) {
      apiIsSet = prefsApiIsSet;
    }

    return apiIsSet;
  }

  // Future<bool> _getSharedPreferences() async {
  //   // var _apiIsSet = false;
  //   // var prefs = await SharedPreferences.getInstance();
  //   // _apiIsSet = prefs.getBool('apiIsSet')!;
  //   // print('_apiIsSet: ${_apiIsSet}');
  //   // setState(() {
  //   //   this._apiIsSet = _apiIsSet;
  //   // });
  //   return _apiIsSet;
  // }
}
