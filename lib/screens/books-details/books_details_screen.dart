import 'dart:io';
import 'dart:typed_data';

import 'package:bookshelves/models/Author.dart';
import 'package:bookshelves/models/Book.dart';
import 'package:bookshelves/providers/download_provider.dart';
import 'package:bookshelves/providers/paths_provider.dart';
import 'package:bookshelves/repositories/BookRepository.dart';
import 'package:bookshelves/screens/ebook_view/ebook_screen.dart';
import 'package:bookshelves/utils/helper.dart';
import 'package:bookshelves/widgets/app_scaffold.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

class BooksDetailsScreen extends StatefulWidget {
  BooksDetailsScreen({Key? key, required this.book}) : super(key: key);
  final Book book;

  @override
  _BooksDetailsScreenState createState() => _BooksDetailsScreenState();
}

class _BooksDetailsScreenState extends State<BooksDetailsScreen> {
  late Future<Book> future;
  late String epubPath =
      '/storage/emulated/0/Download/lhomme-erik_a-comme-association-fr-01_la-pale-lumiere-des-tenebres-fr.epub';
  // late String epubPath;
  // late Uint8List uint8list;

  @override
  void initState() {
    super.initState();
    future = BookRepository().get(widget.book);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.book.title!),
      ),
      body: RefreshIndicator(
        onRefresh: () => _onRefresh(),
        child: FutureBuilder<Book>(
          future: future,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              var book = snapshot.data!;
              return SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 10.0,
                    vertical: 15.0,
                  ),
                  child: Column(
                    children: [
                      Container(
                        child: Column(
                          children: [
                            ListTile(
                              leading: CachedNetworkImage(
                                imageUrl: book.picture!.base!,
                                imageBuilder: (context, imageProvider) =>
                                    Container(
                                  width: 50.0,
                                  height: 50.0,
                                  decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5)),
                                    image: DecorationImage(
                                      image: imageProvider,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                              ),
                              title: Text(book.title!),
                              subtitle: Text(
                                Author.getAuthorList(book.authors!),
                                style: TextStyle(
                                    color: Colors.black.withOpacity(0.6)),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Html(
                                data: book.description!,
                                style: {
                                  'p': Style(
                                    color: Colors.grey.shade700,
                                    textAlign: TextAlign.justify,
                                  ),
                                },
                              ),
                            ),
                            ButtonBar(
                              alignment: MainAxisAlignment.start,
                              children: [
                                OutlinedButton(
                                  onPressed: () async {
                                    // await PathsProvider.getFilesList(
                                    //     dir: PathsProvider
                                    //         .androidExternalDirPath);
                                    // IntentProvider.openFileManager(
                                    //   dir: PathsProvider.androidExternalDirPath,
                                    // );
                                    _downloadEpub(book.epub!.download!);
                                  },
                                  child: Row(
                                    children: [
                                      Icon(Icons.download_rounded),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 3.0),
                                        child: const Text('Download'),
                                      )
                                    ],
                                  ),
                                ),
                                OutlinedButton(
                                  onPressed: () async {
                                    // var list = await _loadFromAssets(epubPath);
                                    // setState(() {
                                    //   uint8list = list;
                                    // });
                                    await Navigator.of(context).push(
                                      MaterialPageRoute(
                                        builder: (context) => EpubReader(
                                          path: epubPath,
                                        ),
                                      ),
                                    );
                                  },
                                  child: Row(
                                    children: [
                                      Icon(Icons.book),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 3.0),
                                        child: const Text('Read'),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ),
      ),
    );
    // return AppScaffold(
    //   title: widget.book.title,
    //   withDrawer: false,
    //   widget: RefreshIndicator(
    //     onRefresh: () => _onRefresh(),
    //     child: FutureBuilder<Book>(
    //       future: future,
    //       builder: (context, snapshot) {
    //         if (snapshot.hasData) {
    //           var book = snapshot.data;
    //           return Container(
    //             margin: const EdgeInsets.all(10.0),
    //             child: Text(book!.title!),
    //           );
    //         } else {
    //           return Center(
    //             child: CircularProgressIndicator(),
    //           );
    //         }
    //       },
    //     ),
    //   ),
    // );
  }

  _onRefresh() {}

  Widget _color(String hexacode) {
    var color = Helper.hexaToColor(hexacode);
    return SizedBox(
      child: DecoratedBox(
        decoration: BoxDecoration(color: color),
      ),
    );
  }

  Future<Uint8List> _loadFromAssets(String path) async {
    // var uint8List = <int>[] as Uint8List;

    // try {
    //   var file = await File(widget.path);
    //   var bytes = await file.readAsBytesSync();
    //   print(bytes);
    //   uint8List = await bytes.buffer.asUint8List();
    //   print(uint8List);
    // } catch (e) {
    //   Log.warn(e.toString());
    // }
    var uint8List = await Uint8List.fromList(File(path).readAsBytesSync());
    // print(uint8List);

    // var assetName = 'assets/book.epub';
    // final bytes = await rootBundle.load(assetName);
    // uint8List = bytes.buffer.asUint8List();

    return uint8List;
  }

  void _downloadEpub(String downloadUrl) async {
    Helper.getMessage(
      context: context,
      text: 'EPUB downloading...',
      noLimit: true,
    );
    var downloadResponse = await DownloadProvider().downloadFile(
      url: downloadUrl,
      directory: PathsProvider.androidExternalDirDownloadPath,
    );
    print(downloadResponse.path);
    print(downloadResponse.filename);
    // setState(() {
    //   epubPath = '${downloadResponse.path}/${downloadResponse.filename}';
    // });
    if (downloadResponse.success) {
      Helper.clearMessage(context: context);
      Helper.getMessage(
        context: context,
        text: 'EPUB downloaded',
        // snackBarAction: SnackBarAction(
        //   label: 'ACTION',
        //   onPressed: () {},
        // ),
      );
    } else {
      Helper.clearMessage(context: context);
      Helper.getMessage(
        context: context,
        text: 'EPUB download error',
      );
    }
  }
}
