import 'dart:convert';

import 'package:bookshelves/bookshelves.dart';
import 'package:bookshelves/routes/app_router.dart';
import 'package:bookshelves/screens/settings/settings_screen.dart';
import 'package:bookshelves/screens/settings/widgets/settings_api_select.dart';
import 'package:bookshelves/widgets/app_scaffold.dart';
import 'package:bookshelves/widgets/input/app_input_text_material.dart';
import 'package:bookshelves/widgets/input/app_radio_list.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsApi extends StatefulWidget {
  final String? api;
  final Map<String, String>? apiList;

  const SettingsApi({Key? key, this.api, this.apiList}) : super(key: key);

  @override
  SettingsApiState createState() {
    return SettingsApiState();
  }
}

class SettingsApiState extends State<SettingsApi> {
  final _newApiController = TextEditingController();
  var _apiValue;
  late Future<List<String>> _futureApiList;

  @override
  void initState() {
    super.initState();
    _futureApiList = _apiList();
  }

  @override
  Widget build(BuildContext context) {
    _apiValue = widget.api;
    return Scaffold(
      appBar: AppBar(
        title: Text('Set API'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 20.0,
                vertical: 15.0,
              ),
              child: Center(
                child: Text(
                  'This application can be used with different API, you can choose default API but you can create your own. You can change it when you want.',
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 20.0,
                vertical: 10.0,
              ),
              child: SizedBox(
                width: double.infinity,
                child: ElevatedButton(
                  onPressed: () {
                    showModalBottomSheet<void>(
                      context: context,
                      builder: (context) {
                        return SettingsApiSelect();
                      },
                    );
                  },
                  style: ButtonStyle(
                    elevation: MaterialStateProperty.all(0),
                  ),
                  child: Text('Select a source'),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<List<String>> _apiList() async {
    var prefs = await SharedPreferences.getInstance();
    var apiListJson = await prefs.getString('apiList');
    var apiListDyn = jsonDecode(apiListJson!);
    var apiList = List<String>.from(apiListDyn);

    return apiList;
  }

  Future<bool> _onBack() async {
    await Navigator.pushReplacementNamed(context, AppRouter.settingsRoute);
    settingsKey.currentState!.setState(() {});
    return true;
  }
}
