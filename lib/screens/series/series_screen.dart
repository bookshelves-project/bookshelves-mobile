import 'package:bookshelves/widgets/app_scaffold.dart';
import 'package:bookshelves/widgets/serie_list.dart';
import 'package:flutter/material.dart';

class SeriesScreen extends StatefulWidget {
  SeriesScreen({Key? key}) : super(key: key);

  @override
  _SeriesScreenState createState() => _SeriesScreenState();
}

class _SeriesScreenState extends State<SeriesScreen> {
  @override
  Widget build(BuildContext context) {
    return SeriesList();
  }
}
