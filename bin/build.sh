#!/bin/bash
echo "= BUILD ="
echo "Username: $1";

case "$1" in
    'aab')
        ./bin/clean.sh
        flutter build appbundle --release
        echo "AAB generated!"
        cp build/app/outputs/bundle/release/app-release.aab ./
        echo "app-release.aab is available at repository root!"
    ;;
    'apk')
        ./bin/clean.sh
        flutter build apk
        echo "APK generated!"
        cp build/app/outputs/flutter-apk/app-release.apk ./
        echo "app-release.apk is available at repository root!"
    ;;
    'apk-split')
        ./bin/clean.sh
        flutter build apk --split-per-abi
        echo "APK generated!"
        cp build/app/outputs/flutter-apk/app-armeabi-v7a-release.apk ./
        echo "app-armeabi-v7a-release.apk is available at repository root!"
    ;;
    'ios')
        ./bin/clean.sh ios
        flutter build ios
        flutter build ipa
    ;;
esac
