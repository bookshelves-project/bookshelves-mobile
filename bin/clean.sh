#!/bin/bash
echo "= CLEAN ="

# if [[ $# -eq 0 ]] ; then
#     echo 'some message'
#     exit 0
# fi

echo $1;

case "$1" in
    ios)
        flutter clean
        rm -Rf ios/Pods
        rm -Rf ios/.symlinks
        rm -Rf ios:Flutter/Flutter.framework
        rm -Rf ios/Flutter/Flutter.podspec
        rm ios/Podfile.lock
        flutter pub get 
        cd ios; \
        pod update; \
        cd ../; \
    ;;
    *)
        flutter clean
	    flutter pub get
    ;;
esac
